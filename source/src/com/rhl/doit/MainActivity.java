package com.rhl.doit;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

public class MainActivity extends Activity implements OnItemClickListener {

	private SwipeDetector swipeDetector;
	private ListView listView;
	private TodoListAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		swipeDetector = new SwipeDetector();

		listView = (ListView) findViewById(R.id.todolist);
		ToDoItem[] values = new ToDoItem[] { new ToDoItem("one", false),
				new ToDoItem("two", false), new ToDoItem("three", false),
				new ToDoItem("four", false), new ToDoItem("five", false),
				new ToDoItem("six", false), new ToDoItem("seven", false),
				new ToDoItem("eight", false), new ToDoItem("nine", false),
				new ToDoItem("ten", false), new ToDoItem("eleven", false),
				new ToDoItem("twelve", false), new ToDoItem("thirteen", false),
				new ToDoItem("fourteen", false), new ToDoItem("fifteen", false),
				new ToDoItem("sixteen", false), new ToDoItem("seventeen", false),
				new ToDoItem("eighteen", false), new ToDoItem("nineteen", false), };

		adapter = new TodoListAdapter(this, R.layout.todo_item, values);
		listView.setAdapter(adapter);

		listView.setOnTouchListener(swipeDetector);
		listView.setOnItemClickListener(this);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View view, int position,
			long arg3) {
		if (swipeDetector.swipeDetected()) {

			switch (swipeDetector.getAction()) {
			case LEFT_TO_RIGHT:

				adapter.getItem(position).isChecked = true;
				adapter.notifyDataSetChanged();

				break;
			case RIGHT_TO_LEFT:

				adapter.getItem(position).isChecked = false;
				adapter.notifyDataSetChanged();

				break;

			default:
				break;
			}

		} else {
			openTask();
		}
	}
	
	public void openTask() {
	    Intent intent = new Intent(this, TaskDetailActivity.class);
	    startActivity(intent);
	}
}
