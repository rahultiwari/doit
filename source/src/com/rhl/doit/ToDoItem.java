package com.rhl.doit;

public class ToDoItem {
	public String text;
	public boolean isChecked;

	public ToDoItem(String text, boolean isChecked) {
		super();
		this.text = text;
		this.isChecked = isChecked;
	}
}
