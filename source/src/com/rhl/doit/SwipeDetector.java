package com.rhl.doit;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

public class SwipeDetector implements View.OnTouchListener {

    public static enum Action {
        LEFT_TO_RIGHT, // Left to Right
        RIGHT_TO_LEFT, // Right to Left
        TOP_TO_BOTTOM, // Top to bottom
        BOTTOM_TO_TOP, // Bottom to Top
        NONE // when no action was detected
    }

    private static final String logTag = "SwipeDetector";
    private static final int HORIZONTAL_MIN_DISTANCE = 60;
	private static final float VERTICAL_MIN_DISTANCE = 50;
    private float downX, downY, upX, upY;
    private Action mSwipeDetected = Action.NONE;

    public boolean swipeDetected() {
        return mSwipeDetected != Action.NONE;
    }

    public Action getAction() {
        return mSwipeDetected;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
    	switch (event.getAction()) {
        case MotionEvent.ACTION_DOWN: {
            downX = event.getX();
            downY = event.getY();
            mSwipeDetected = Action.NONE;
            return false; // allow other events like Click to be processed
        }
        case MotionEvent.ACTION_MOVE: {
            upX = event.getX();
            upY = event.getY();

            float deltaX = downX - upX;
            float deltaY = downY - upY;

            // horizontal swipe detection
                    if (Math.abs(deltaX) > HORIZONTAL_MIN_DISTANCE) {
                        // left or right
                        if (deltaX < 0) {
                            Log.i(logTag, "Swipe Left to Right");
                            mSwipeDetected = Action.LEFT_TO_RIGHT;
                            return true;
                        }
                        if (deltaX > 0) {
                            Log.i(logTag, "Swipe Right to Left");
                            mSwipeDetected = Action.RIGHT_TO_LEFT;
                            return true;
                        }
                    } else 

                    // vertical swipe detection
                    if (Math.abs(deltaY) > VERTICAL_MIN_DISTANCE) {
                        // top or down
                        if (deltaY < 0) {
                            Log.i(logTag, "Swipe Top to Bottom");
                            mSwipeDetected = Action.TOP_TO_BOTTOM;
                            return false;
                        }
                        if (deltaY > 0) {
                            Log.i(logTag, "Swipe Bottom to Top");
                            mSwipeDetected = Action.BOTTOM_TO_TOP;
                            return false;
                        }
                    } 
                    return true;
        }
        }
        return false;
    }
}
