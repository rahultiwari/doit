package com.rhl.doit;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class TodoListAdapter extends ArrayAdapter<ToDoItem> {
	private int layoutResourceId;
	private ToDoItem data[];

	public TodoListAdapter(Context context, int layoutResourceId,
			ToDoItem[] data) {
		super(context, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.data = data;
	}

	/*
	 * information holder
	 */
	static class InformationHolder {
		TextView text;
		boolean isChecked;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		InformationHolder holder = null;
		Context context = getContext();

		if (row == null) {
			LayoutInflater inflater = LayoutInflater.from(context);
			row = inflater.inflate(this.layoutResourceId, parent, false);

			holder = new InformationHolder();
			holder.text = (TextView) row.findViewById(R.id.todo_item_text);
			row.setTag(holder);
		} else {
			holder = (InformationHolder) row.getTag();
		}
		holder.text.setText(data[position].text);
		holder.isChecked = data[position].isChecked;
		if (holder.isChecked) {
			holder.text.setPaintFlags(holder.text.getPaintFlags()
					| Paint.STRIKE_THRU_TEXT_FLAG);
		} else {
			holder.text.setPaintFlags(holder.text.getPaintFlags()
					& (~Paint.STRIKE_THRU_TEXT_FLAG));
		}

		return row;
	}

}
